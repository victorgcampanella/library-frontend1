import React from 'react';
import { Switch } from 'react-router-dom';
import Route from './Route';

import SignIn from '../pages/SignIn';
import SignUp from '../pages/SignUp';
import Books from '../pages/Books';
import BooksLib from '../pages/BooksLib';

function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={SignIn} />
      <Route path="/register" component={SignUp} />
      <Route path="/books" component={Books} isPrivate />
      <Route path="/bookslib" component={BooksLib} isPrivate />
    </Switch>
  );
}


export default Routes;
